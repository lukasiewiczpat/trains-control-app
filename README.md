# Trains control app

Trains control app was developed in order to control toy trains with a R421B16 electronic circuit board. This application allows for manual switching relays state or running a sequence given with ```.csv ``` file. Each sequence file needs to follow a special structure which is shown and described in ```sequence_test_file.csv```. A multithreading solution was implemented to ensure an appropriate functionality. There are up to three threads running simultaneously:

- First thread runs a tkinter window which allows for constant user and application interaction.
- Second thread reads periodically an actual relays state using communication. Reading thread can work constantly because each sending operation blocks reading.
- Third thread is started when sequence is initialized. It is only active when sequence is running.

## Prerequisites

To run the application Python 3 and Tkinter is required. Official Python distribution is available [here](https://www.python.org/ "Python wabsite"). There are two additional modules which have to be installed before: *pandas* and *pillow*. They can be installed by *pip install* command in a command line.
```
pip install pandas
```
and
```
pip install pillow
```
Last but (for sure) not least prerequisite is hardware. The R421B16 electronic circuit board has to be connected via COM port (the board support RS485 standard, so some USB to RS485 converter will be probably needed). A photo of the R421B16 was shown below. 

![photo_1](https://gitlab.com/lukasiewiczpat/trains-control-app/-/raw/master/Photos/photo_1.png)

## Quick start

When all prerequisites are fulfilled, adjust settings in ```config.json``` file. There are only three values which should be specified:

- port COM which is used to communicate with the board,
- board's RS485 address corresponding to DIP switches position,
- tkinter window size given in pixels (format *[width]x[height]* is required).

Example configuration is shown below:

```json
{
    "SERIAL_PORT": "COM3",
    "BOARD_ADDRESS": 1,
    "WINDOW_SIZE": "1000x600"
}
```

## Credits

This app bases on Erriez's repository where RS485, 8-channel board is used. Special thanks for great piece of work which is used as library for this project. Erriez's repository is available [here](https://github.com/Erriez/R421A08-rs485-8ch-relay-board "Erriez's repository").

## License

Trains control app is distributed under the MIT License, see the [LICENSE](https://gitlab.com/lukasiewiczpat/trains-control-app/-/blob/master/LICENSE "LICENSE") file.

## Screenshots

### console
![console](https://gitlab.com/lukasiewiczpat/trains-control-app/-/raw/master/Screenshots/console.png)

### tkinter window
![tkinter_window](https://gitlab.com/lukasiewiczpat/trains-control-app/-/raw/master/Screenshots/tkinter_window.png)

## YouTube video

[![Trains control app on YouTube](https://gitlab.com/lukasiewiczpat/trains-control-app/-/raw/master/Gifs/trains_control_app.gif)](https://youtu.be/GHkoGSV6Vv4)
