'''
classes for trains_control_app.py

'''

import numpy as np
import time
import _thread
import tkinter as tk
from tkinter import filedialog
import math
import pandas as pd
from PIL import Image, ImageTk
import os.path


class board_copy(object):

    
    def __init__(self, board):
        '''
        Contrustructor.
        ---
        Parameters:
        board (object): board type object based on library structure.
        ---
        Attributes:
        _channels_status_list (list): 1-demensional list, length: 16. Each element contains channel's status -> 0 - no status
                                                                                                                1 - off
                                                                                                                2 - on
        _board (object): reference to object based on library, allows to use R421B16 board
        _reading_loop_active (Bool): is activated when reading thread is started. When it's change to False, reading thread stops
        _reading_blocked (Bool): when it changes to True, reading coils status is immediately stopped. This bool value is used for blocking reading actual coils status in order to allow sending as quick as possible.
        '''

        self._channels_status_list=list(np.zeros(16, dtype=int))
        self._board=board
        self._reading_loop_active=False
        self._reading_blocked=False


    def update_channels_status(self):
        '''
        Method update self._channels_status_list via reading values using modbus. Reading is blocked immediately if some sending action is performed in other thread.
        '''
        
        for i in range(1,17):

            if not self._reading_blocked:
                #reading is always blocked when some write action is in progress                                            
                self._channels_status_list[i-1]=self._board.check_relay_status(relay=i)
                time.sleep(0.05)

                if i==16:
                    print(f"all _channels_status_list was updated via modbus successfully:\n{self._channels_status_list}")
            else:
                break

        
    def reading_loop_method(self, wait_time):
        '''
        reading_loop_method is started as new thread. Method constanly reads actual values via modbus and updates self._channels_status_list using method update_channels_status.
        Reading thread can work constantly because each sending operation blocks reading. In general, this method was developed to check real relays values periodically.
        There is no need to refresh values quickly because each sending operation updates appropriate element in self._channels_status_list.
        ---
        Parameters:
        wait_time (float): time wait after update (or break) self._channels_status_list
        '''

        print("Starting reading loop method")
        
        while True:
            if self._reading_loop_active:
                self.update_channels_status()
                time.sleep(wait_time)
            else:
                break


    def start_reading_thread(self, wait_after=1.0):
        '''
        Method starts reading thread which periodically checks real relays value.
        ---
        Parameters:
        wait_after (float): this argument is passed to reading_loop_method. Value determines waiting time after reading state of all relays (or break reading).
        '''

        self._reading_loop_active=True

        _thread.start_new_thread(self.reading_loop_method, (wait_after, ))


    def stop_reading_thread(self):
        '''
        Method stops thread which reads relays values.
        '''
        self._reading_loop_active=False


    def toggle_relay(self, relay):
        '''
        Method switches state of chosen relay.
        ---
        Parameters:
        relay (int): number of relay, integer value 1-16
        '''

        self._reading_blocked=True #first block reading from board
        time.sleep(0.04) #wait, to be sure that another thread stop reading from board

        self._board.toggle(relay=relay) #send toggle command

        #update self._channels_status_list
        if self._channels_status_list[relay-1]==1:
           self._channels_status_list[relay-1]=2
        elif self._channels_status_list[relay-1]==2:
            self._channels_status_list[relay-1]=1

        time.sleep(0.01)

        self._reading_blocked=False #allow reading again


    def on_relay(self, relay):
        '''
        Method turns on chosen relay.
        ---
        Parameters:
        relay (int): number of relay, integer value 1-16
        '''

        self._reading_blocked=True #first block reading from board
        time.sleep(0.06) #wait, to be sure that another thread stop reading from board

        self._board.on(relay=relay) #send on command

        #update self._channels_status_list
        self._channels_status_list[relay-1]=2

        time.sleep(0.01)

        self._reading_blocked=False #allow reading again


    def off_relay(self, relay):
        '''
        Method turns off chosen relay.
        ---
        Parameters:
        relay (int): number of relay, integer value 1-16
        '''
        
        self._reading_blocked=True #first block reading from board
        time.sleep(0.06) #wait, to be sure that another thread stop reading from board

        self._board.off(relay=relay) #send on command

        #update self._channels_status_list
        self._channels_status_list[relay-1]=1

        time.sleep(0.01)

        self._reading_blocked=False #allow reading again
        


class window(object):


    def __init__(self, board_copy, window_size):
        '''
        Initialize tkinter window with all needed functionality.
        ---
        Parameters:
        board_copy (object): give access to board methods and attributes
        window_size (string): specifies tkinter window size: format [width]x[hight]
        ---
        Attributes:
        board_copy (object): reference to board_copy object
        sequence (object): sequence class object, allows for load sequence file and run sequence
        window (object): tkinter window
        relays_buttons_list (list): list of objects (tkinter buttons) length 16.
                                    Each element is connected to apropriate toggle switch, which can chanege relay state.
        open_and_load_button (tkinter button object):
        info_msg_label (tkinter label object):
        start_button (tkinter button object):
        stop_button (tkinter button object):
        logo_image (ImageTk.PhotoImage object):
        footer_label (tkinter label object):
        '''
        self.board_copy=board_copy

        #initialize sequence
        self.sequence=sequence(self, self.board_copy)

        #Create tkinter window object and set all parameters
        self.window=tk.Tk() #tkinter object
        self.window.title("Trains control app")
        self.window.geometry(window_size)
        self.window.resizable(0, 0) #block geometry
        
        #Create 16 relays buttons list
        self.relays_buttons_list=[]

        #window_size format "width"x"heigth"
        window_h=int(window_size[window_size.find('x')+1:])
        window_w=int(window_size[:window_size.find('x')])

        #button and spaces cover 60% of window heigh where 70% is covered by buttons and 30% by spaces. There are 2 rows of buttons and 3 rows of spaces
        button_h=int(window_h*(0.6)*(0.7)/2) 
        space_h=int(window_h*(0.6)*(0.3)/3)
        #button and spaces cover 100% of window width where 90% is covered by buttons and 10% by spaces. There are 8 columns of buttons and 9 columns of spaces
        button_w=int(window_w*(0.9)/8)
        space_w=int(window_w*(0.1)/8)

        #Create 16 buttons to toggle relays
        for i in range(1,17):

            x_pos=space_w+(((i-1)%8)*(space_w+button_w))*(math.floor(((i-1)%8)/8)+1)
            y_pos=space_h+math.floor((i-1)/8)*(space_h+button_h)

            self.relays_buttons_list.append(tk.Button(self.window, text=f"relay{i}\n", borderwidth=4, font="arial 10", relief=tk.RIDGE)) #list of buttons objects        
            #self.relays_buttons_list[i-1].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, i)) #Doesn't work inside loop
            self.relays_buttons_list[i-1].place(width=button_w, height=button_h, x=x_pos, y=y_pos)

        #Bind outside loop because it doesn't work inside for loop        
        self.relays_buttons_list[0].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 1))
        self.relays_buttons_list[1].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 2))
        self.relays_buttons_list[2].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 3))
        self.relays_buttons_list[3].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 4))
        self.relays_buttons_list[4].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 5))
        self.relays_buttons_list[5].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 6))
        self.relays_buttons_list[6].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 7))
        self.relays_buttons_list[7].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 8))
        self.relays_buttons_list[8].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 9))
        self.relays_buttons_list[9].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 10))
        self.relays_buttons_list[10].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 11))
        self.relays_buttons_list[11].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 12))
        self.relays_buttons_list[12].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 13))
        self.relays_buttons_list[13].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 14))
        self.relays_buttons_list[14].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 15))
        self.relays_buttons_list[15].bind('<ButtonRelease-1>', lambda e:self.button_command_relay_toggle(e, 16))

        #Set buttons colors and text
        self.update_all_buttons_color()
        self.update_all_buttons_text()

        #Open and load file button
        self.open_and_load_button=tk.Button(self.window, text=f"Open and load sequence", borderwidth=4, font="arial 10", relief=tk.RIDGE)
        self.open_and_load_button.place(width=0.2*window_w, height=0.06*window_h, x=0.15*window_w, y=(0.6+0.02)*window_h)
        self.open_and_load_button.bind('<ButtonRelease-1>', self.sequence.load_file)        

        #Info msg label
        self.info_msg_label=tk.Label(self.window, text="STATUS INFO: "+self.sequence._msg_info, font="arial 11")
        self.info_msg_label.place(width=0.4*window_w, height=0.04*window_h, x=0.05*window_w, y=(0.71)*window_h)

        #Start button
        self.start_button=tk.Button(self.window, text=f"RUN\nSEQUENCE", borderwidth=4, font="arial 14", relief=tk.RIDGE, fg="#3ab56f")
        self.start_button.configure(state=tk.DISABLED)
        self.start_button.place(width=0.15*window_w, height=0.14*window_h, x=0.08*window_w, y=(0.78)*window_h)
        self.start_button.bind('<ButtonRelease-1>', self.sequence.start_sequence)  

        #Stop button
        self.stop_button=tk.Button(self.window, text=f"STOP\nSEQUENCE", borderwidth=4, font="arial 14", relief=tk.RIDGE, fg="#e06565")
        self.stop_button.configure(state=tk.DISABLED)
        self.stop_button.place(width=0.15*window_w, height=0.14*window_h, x=0.27*window_w, y=(0.78)*window_h)
        self.stop_button.bind('<ButtonRelease-1>', self.sequence.stop_sequence)

        #Image
        image_w=int(0.44*window_w) #image width is 44% of window width
        image_h=int(0.44*window_w*(567/1400)) #appropriate height not to deformate image
        image_path= os.path.join(os.path.dirname(os.path.realpath(__file__)),"img","trains_control_app_logo.png")
        image_temp = Image.open(image_path).convert("RGBA")        
        image_temp = image_temp.resize((image_w, image_h), Image.ANTIALIAS)
        self.logo_image = ImageTk.PhotoImage(image_temp) #tkinter type
        self.logo_image_label = tk.Label(self.window, image=self.logo_image)
        self.logo_image_label.photo = self.logo_image
        self.logo_image_label.place(x=0.51*window_w, y=0.62*window_h) #place 51% from right and 62% from top

        #Footer Label
        self.footer_label=tk.Label(self.window, text="MIT License   ||   Copyright (c) 2020 Patryk Łukasiewicz", font="arial 9")
        self.footer_label.place(width=0.8*window_w, height=0.03*window_h, x=0.1*window_w, y=(0.96)*window_h)
        

    def run_window(self):
        '''
        Method starts tkinter window and blocks program main loop.
        '''
        self.window.mainloop()


    def button_command_relay_toggle(self, event, relay_number):
        '''
        Method is called as event, when one of toggle switch is pressed. It changes state of appriopriate relay.
        ---
        Parameters:
        relay_number (int): specifies number of relay which gonna be change (1-16)
        '''

        if self.relays_buttons_list[relay_number-1]["state"]!=tk.DISABLED:
            self.board_copy.toggle_relay(relay=relay_number)
            print(f"Relay {relay_number} was toggled manually")
            self.update_all_buttons_color()
            self.update_all_buttons_text()


    def update_all_buttons_color(self):
        '''
        Method sets color (red or green) based on switch state (self.board_copy._channels_status_list).
        '''

        for i in range(16):

            if self.board_copy._channels_status_list[i]==1:
                #relay is turned off
                self.relays_buttons_list[i].configure(background="#fc7272")

            elif self.board_copy._channels_status_list[i]==2:
                #relay is turned on
                self.relays_buttons_list[i].configure(background="#54fa9b")

            #if state is 0 (no_status) do not set color


    def update_all_buttons_text(self):
        '''
        Method sets text ('OFF' or 'ON') based on switch state (self.board_copy._channels_status_list).
        '''

        for i in range(16):

            if self.board_copy._channels_status_list[i]==1:
                #relay is turned off
                self.relays_buttons_list[i].configure(text=f"relay {i+1}\nOFF")

            elif self.board_copy._channels_status_list[i]==2:
                #relay is turned on
                self.relays_buttons_list[i].configure(text=f"relay {i+1}\nON")

            else:
                #unknown state
                self.relays_buttons_list[i].configure(text=f"relay {i+1}")


    def update_info_msg(self, msg):
        '''
        Method updates status information with given message
        ---
        Parameters:
        msg (string): displayed message
        '''

        self.info_msg_label.configure(text="STATUS INFO: "+msg)


    def disable_all_relays_buttons(self):
        '''
        Method makes all toggle switches disabled. It's used when sequence starts.      
        '''

        for button in self.relays_buttons_list:
            button.configure(state=tk.DISABLED)

    
    def enable_all_relays_buttons(self):
        '''
        Method makes all toggle switches enabled. It's used when sequence ends.
        '''
        
        for button in self.relays_buttons_list:
            button.configure(state=tk.NORMAL)



class sequence(object):
    

    def __init__(self, window, board_copy):
        '''
        Constructor
        ---
        Atributes:
        _state (int): Sequence state -> 1: no_file_loaded
                                        2: file_loaded
                                        3: sequence_running
                                        9: error
        _msg_info (string): info message shown in tkinter window
        _loaded_file_list (list): list of list which contains sequence -> [[time(float), relay(int), function_number(int), comment(string)], ...]
        _window (object): reference to tkinter window, allows for use it's methods
        _board_copy (object): reference to board_copy object, allows for use it's methods
        _sequence_running (bool): it is True when sequence is running and False when it's stopped. This value helps to manage sequence thread.
        _tk_window_closed (bool): it is True when tkinter window is closed. This value shut down sequence thread and block functions which are connected to tkinter window.
        '''

        self._state=1
        self._msg_info="No file loaded"
        self._loaded_file_list=[]
        self._window=window
        self._board_copy=board_copy
        self._sequence_running=False
        self._tk_window_closed=False


    def load_file(self, event):
        '''
        Method is called as button event. It loads .csv file to list which can be easly run as sequence.        
        '''

        temp_loaded_file_list=[]

        file_path=filedialog.askopenfilename(filetypes=[("CSV files", ".csv")])

        if file_path:
            #if open was pressed

            print(f"Choosen file: {file_path}")

            #try to open choosen file
            try:
                data_frame=pd.read_csv(file_path, sep=';', header=None)
                file_opened=True
                print("File loaded")
            except:
                self._msg_info="Can't open choosen file"
                self._window.update_info_msg(self._msg_info)
                file_opened=False
                print("Can't open choosen file")

            #try to decode .csv file
            analysis_ok=True

            if file_opened==True:
                
                try:
                    #convert opened file to sequence list and check it's content
                    raw_sequence_list=data_frame.values.tolist()

                    for line in raw_sequence_list:
                        new_line=[]
                        
                        #check if 1st value of line can be convert to float, if not excpetion occured
                        new_line.append(float(line[0]))

                        #check if 2nd value is relay number 1-16, try to convert to int if can't exception occured:
                        if line[1].find('R')!=-1 and int(line[1][line[1].find('R')+1:])>=1 and int(line[1][line[1].find('R')+1:])<=16:
                            #if "R" is given
                            new_line.append(int(line[1][line[1].find('R')+1:]))
                        else:
                            self._msg_info=f"Can't analyse .csv file (check: {new_line[0]}s of sequence)"
                            self._window.update_info_msg(self._msg_info)
                            analysis_ok=False
                            break

                        #check if 3rd value is function number 1, 2 or 3, try to convert to int if can't exception occured:
                        if line[2].find('F')!=-1 and int(line[2][line[2].find('F')+1:])>=1 and int(line[2][line[2].find('F')+1:])<=3:
                            #if "F" is given
                            new_line.append(int(line[2][line[2].find('F')+1:]))
                        else:
                            self._msg_info=f"Can't analyse .csv file (check: {new_line[0]}s of sequence)"
                            self._window.update_info_msg(self._msg_info)
                            analysis_ok=False
                            break

                        temp_loaded_file_list.append(new_line)

                except:
                    self._msg_info=f"Can't analyse .csv file, exception occured"
                    self._window.update_info_msg(self._msg_info)
                    analysis_ok=False

            if analysis_ok==True:
                #if analysis is done correctly
                self._loaded_file_list=temp_loaded_file_list
                self._state=2
                self._msg_info=f"File loaded"
                self._window.update_info_msg(self._msg_info)
                #unlock start button
                self._window.start_button.configure(state=tk.NORMAL)


    def start_sequence(self, event):
        '''
        Method is called as button event. It starts new thread which realizes sequence.
        '''

        if self._window.start_button["state"]!=tk.DISABLED:
            
            #start new thread - sequence
            self._sequence_running=True
            _thread.start_new_thread(self.sequence, ())


    def stop_sequence(self, event):
        '''
        Method is called as button event. It changes self._sequence_running to False what shuts down sequence thread.
        '''

        if self._window.stop_button["state"]!=tk.DISABLED:

            #stop sequence thread
            self._sequence_running=False


    def stop_sequence_by_app(self):
        '''
        Method changes self._tk_window_closed to True what shuts down sequence and blocks functions which are connected to tkinter window. 
        '''

        #stop sequence thread
        self._tk_window_closed=True


    def sequence(self):
        '''
        sequence method is started as new thread. Method realizes sequence specified by self._loaded_file_list.
        '''

        #buttons enable-disable
        self._window.start_button.configure(state=tk.DISABLED)
        self._window.stop_button.configure(state=tk.NORMAL)
        self._window.disable_all_relays_buttons()

        #start sequence
        self._state=3
        self._msg_info=f"Sequence is running."
        self._window.update_info_msg(self._msg_info)

        start_time=time.time()
        line_number=int(0)

        while True:
            if self._sequence_running and self._tk_window_closed==False:


                if line_number<len(self._loaded_file_list):
                    #if it isn't end of sequence
                        elapsed_time=time.time()-start_time

                        if elapsed_time>=self._loaded_file_list[line_number][0]:
                            #if it's time to realize line
                            
                            if self._loaded_file_list[line_number][2]==1:
                                #ON function
                                self._board_copy.on_relay(relay=self._loaded_file_list[line_number][1])
                                line_number+=1 

                            elif self._loaded_file_list[line_number][2]==2:
                                #OFF function
                                self._board_copy.off_relay(relay=self._loaded_file_list[line_number][1])
                                line_number+=1 

                            elif self._loaded_file_list[line_number][2]==3:
                                #TOGGLE function
                                self._board_copy.toggle_relay(relay=self._loaded_file_list[line_number][1])
                                line_number+=1 

                            else:
                                #UNKNOWN function - ERROR
                                if self._tk_window_closed==False:
                                    self._state=9
                                    self._msg_info=f"Error while running file."
                                    self._window.update_info_msg(self._msg_info)

                                break

                else:

                    #end of seqence
                    if self._tk_window_closed==False:
                        self._state=2
                        self._msg_info=f"Sequence done."
                        self._window.update_info_msg(self._msg_info)

                    break

            else:                
                #sequence stopped
                if self._tk_window_closed==False:
                    self._state=2
                    self._msg_info=f"Sequence stopped."
                    self._window.update_info_msg(self._msg_info)

                break

            if self._tk_window_closed==False:
                self._window.update_all_buttons_color()
                self._window.update_all_buttons_text()

            time.sleep(0.02)

        #buttons enable-disable
        if self._tk_window_closed==False:
            self._window.start_button.configure(state=tk.NORMAL)
            self._window.stop_button.configure(state=tk.DISABLED)
            self._window.enable_all_relays_buttons()