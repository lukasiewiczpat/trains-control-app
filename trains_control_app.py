'''
trains_control_app allows for control of R421B16 board via COM port. This application was prepared to use the board for control toy trains.

***
Credits:
This app bases on Erriez's repository where RS485, 8-channel board is used. Special thanks for great piece of work, which is used as library for this project.

Source: https://github.com/Erriez/R421A08-rs485-8ch-relay-board

'''
 

# --- INCLUDE ---
import time
import json
import sys
import os.path
import classes
import _thread

# Add system path to find relay_ Python packages
# Absolute path - Python version
app_path=os.path.dirname(os.path.realpath(__file__))
lib_path= os.path.join(os.path.dirname(os.path.realpath(__file__)),"lib","R421A08-rs485-8ch-relay-board-master")


sys.path.append(lib_path)

import relay_modbus
import relay_boards


# --- BOARD CONNECTION ---
# Read configuration from 'config.json'
try:
    config_file=open(os.path.join(app_path, "config.json"))
    config_file_dict=json.load(config_file)
    SERIAL_PORT=config_file_dict["SERIAL_PORT"]
    board_address=config_file_dict["BOARD_ADDRESS"]
    window_size=config_file_dict["WINDOW_SIZE"]
except:
    print("Can't open configuration file. Quitting...")
    time.sleep(2)
    sys.exit(1)

# Create MODBUS object
_modbus = relay_modbus.Modbus(serial_port=SERIAL_PORT)

# Open serial port
try:
    _modbus.open()
except relay_modbus.SerialOpenException as err:
    print("Can't open serial port. Quitting...")
    time.sleep(2)
    sys.exit(1)

# Create relay board object (real board type is R421B16, R421A08 name origin is from library)
board = relay_boards.R421A08(_modbus, address=board_address)

board_copy=classes.board_copy(board=board) # create board_copy object for display purposes

print("\n")
print("*** Communication between application and board was established successfully ***")


# --- START READING THREAD ---
board_copy.start_reading_thread(wait_after=1)

print("Waiting for initial update of board_copy._channels_status_list...\n")
time.sleep(3)#wait for update 


# --- INITIALIZE AND START TKINTER WINDOW ---
window=classes.window(board_copy, window_size=window_size)
window.run_window()


# --- TKINTER WINDOW CLOSED ---
#close threads
window.sequence.stop_sequence_by_app()
board_copy.stop_reading_thread()

#turn off all relays
for i in range(1,17):
    board_copy.off_relay(i)